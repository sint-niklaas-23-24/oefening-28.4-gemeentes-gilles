﻿namespace Oefening_28._4___Gemeentes
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Provincie provincie = new Provincie("Antwerpen");
            Gemeente gemeente1 = new Gemeente("2460", "Kasterlee");
            Gemeente gemeente2 = new Gemeente("2440", "Geel");
            Gemeente gemeente3 = new Gemeente("1000", "Brussel");
            Gemeente gemeente4 = new Gemeente("2275", "Lille");
            provincie.AddGemeente(gemeente1);
            provincie.AddGemeente(gemeente2);
            provincie.AddGemeente(gemeente3);
            provincie.AddGemeente(gemeente4);
            Console.WriteLine(provincie.ToString());
            provincie.RemoveGemeente(gemeente3);
            Console.WriteLine("Na het verwijderen van Brussel.");
            Console.WriteLine(provincie.ToString());
        }
    }
}