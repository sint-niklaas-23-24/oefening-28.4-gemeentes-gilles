﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oefening_28._4___Gemeentes
{
    internal class Provincie
    {
        private string _provincienaam;
        private List<Gemeente> _gemeentes = new List<Gemeente>();

        public Provincie(string provincie)
        {
            this.Provincienaam = provincie;
            Gemeentes = new List<Gemeente>();
        }

        public string Provincienaam
        {
            get { return _provincienaam; }
            set { _provincienaam = value; }
        }
        private List<Gemeente> Gemeentes
        {
            get { return _gemeentes; }
            set { _gemeentes = value; }
        }

        public void AddGemeente(Gemeente eenGemeente)
        {
            Gemeentes.Add(eenGemeente);
        }
        public void RemoveGemeente(Gemeente eenGemeente)
        {
            Gemeentes.Remove(eenGemeente);
        }
        public override string ToString()
        {
            string resultaat = $"Provincie {Provincienaam}" + Environment.NewLine;
            foreach (Gemeente eenGemeente in Gemeentes)
            {
                resultaat += $"{eenGemeente.Postcode} - {eenGemeente.GemeenteNaam}" + Environment.NewLine;
            }
            return resultaat;
        }
    }
}
