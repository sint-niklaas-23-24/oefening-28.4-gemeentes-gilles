﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oefening_28._4___Gemeentes
{
    internal class Gemeente
    {
        private string _gemeenteNaam;
        private string _postcode;

        public Gemeente(string postcode, string gemeente) 
        { 
            this.Postcode = postcode;
            this.GemeenteNaam = gemeente;
        }

        public string GemeenteNaam
        {
            get { return _gemeenteNaam; }
            set { _gemeenteNaam = value; }
        }
        public string Postcode
        {
            get { return _postcode; }
            set { _postcode = value; }
        }

        public override bool Equals(object? obj)
        {
            bool resultaat = false;
            //3 dingen doen:
            //1. Controleren of mijn object niet gelijk is aan null. COPY PASTE
            if (obj != null)
            {
                //2. Controleren of het datatype gelijk is aan elkaar. COPY PASTE
                if (GetType() == obj.GetType())
                {
                    //3. Eigen validatie
                    Gemeente gemeente = (Gemeente)obj;
                    if (this.Postcode == gemeente.Postcode && this.GemeenteNaam == gemeente.GemeenteNaam)
                    {
                        resultaat = true;
                    }
                }
            }
            return resultaat;
        }
        public override string ToString()
        {
            return $"{Postcode} - {GemeenteNaam}" + Environment.NewLine;
        }
    }
}
